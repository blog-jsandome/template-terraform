terraform {

  required_providers {

    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {

  region     = var.aws_region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key

}

data "aws_vpc" "argo" {
  tags = {
    Name = "${var.app_name}-vpc-${var.app_environment}"
  }
}


module "sg_alb" {
  source          = "../../../modules/security_group"
  app_name        = var.app_name
  app_environment = var.app_environment
  resource        = "sg-alb-development"
  project         = "argo"
  id_vpc          = data.aws_vpc.argo.id
}

# Ingress rules
module "sg_rule_ingress_443" {
  source           = "../../../modules/security_group/ingress_rule"
  from_port        = 443
  to_port          = 443
  protocol         = "tcp"
  cidr_blocks      = ["0.0.0.0/0"]
  ipv6_cidr_blocks = ["::/0"]
  sg_id            = module.sg_alb.security_id
}

module "sg_rule_ingress_80" {
  source           = "../../../modules/security_group/ingress_rule"
  from_port        = 80
  to_port          = 80
  protocol         = "tcp"
  cidr_blocks      = ["0.0.0.0/0"]
  ipv6_cidr_blocks = ["::/0"]
  sg_id            = module.sg_alb.security_id
}

# Egress rules 

module "sg_rule_default" {
  source = "../../../modules/security_group/egress_rule"
  sg_id  = module.sg_alb.security_id
}
