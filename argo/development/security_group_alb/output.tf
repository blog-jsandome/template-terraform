

output "id_vpc" {
  value = module.sg_alb.id_vpc
}

output "security_id" {
  value = module.sg_alb.security_id
}
