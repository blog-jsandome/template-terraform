terraform {

  required_providers {

    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {

  region     = var.aws_region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key

}

module "alb" {
  source          = "../../../modules/alb"
  app_name        = var.app_name
  app_environment = var.app_environment
  resource        = "alb-ecs-development"
  public_subnets  = data.aws_subnet.public_selected.*.id
  security_groups = [data.aws_security_group.sg_alb.id]

}

module "target_group" {
  source          = "../../../modules/target_group_alb"
  id_vpc          = data.aws_vpc.argo.id
  app_name        = var.app_name
  app_environment = var.app_environment
  resource        = "tg-alb-ecs-development"
  path            = "/"
  matcher         = "200"
}

module "target_group_dos" {
  source          = "../../../modules/target_group_alb"
  id_vpc          = data.aws_vpc.argo.id
  app_name        = var.app_name
  app_environment = var.app_environment
  resource        = "tg-alb-ecs-web-development"
  path            = "/"
  matcher         = "200"
  port            = "801"
}

module "listener_http_redirect" {
  source   = "../../../modules/listener_alb_redirect"
  alb_arn  = module.alb.alb_arn
  port     = "80"
  protocol = "HTTP"

}

module "listener_https" {
  source           = "../../../modules/listener_alb"
  alb_arn          = module.alb.alb_arn
  port             = 443
  protocol         = "HTTPS"
  ssl_policy       = "ELBSecurityPolicy-2016-08"
  cert_arn         = var.certificate_argo
  target_group_arn = module.target_group.tg_id
}

module "rule_website" {
  source           = "../../../modules/rule-alb-host-based"
  listener_arn     = module.listener_https.alb_listener_arn
  priority         = 99
  target_group_arn = module.target_group_dos.tg_id
  host_header      = "development-website.argovisa.com"

}

