# Get VPC was created before execute this data

data "aws_vpc" "argo" {
  tags = {
    Name = "${var.app_name}-vpc-${var.app_environment}"
  }
}

# Get Public subnets with the tag Tier

data "aws_subnet_ids" "public" {
  vpc_id = data.aws_vpc.argo.id
  tags = {
    Tier = "public"
  }
}

data "aws_subnet" "public_selected" {
  count = length(data.aws_subnet_ids.public.ids)
  id    = tolist(data.aws_subnet_ids.public.ids)[count.index]
}

data "aws_security_group" "sg_alb" {
  tags = {
    Name = "${var.app_name}-sg-alb-development"
  }
}
