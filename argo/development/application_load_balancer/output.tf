
output "alb_id" {
  value = module.alb.alb_id
}

output "alb_arn" {
  value = module.alb.alb_arn
}

output "tg_id" {
  value = module.target_group.tg_id
}

#output "alb_listener" {
#  value = module.listener_http.alb_listener
#}

output "alb_listener_arn" {
  value = module.listener_http_redirect.alb_listener_arn
}
