terraform {

  required_providers {

    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {

  region     = var.aws_region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key

}

module "s3_vars_ecs" {
  source      = "../../../modules/s3-ecs-vars"
  name_bucket = var.name_bucket

}

module "objects_s3_vars" {
  source      = "../../../modules/s3-ecs-vars/bucket-object"
  bucket_id   = module.s3_vars_ecs.bucket_id
  source_file = "vars-argo.env"
}

module "objects_s3_vars_web" {
  source      = "../../../modules/s3-ecs-vars/bucket-object"
  bucket_id   = module.s3_vars_ecs.bucket_id
  source_file = "vars-argo-web.env"
  key         = "web.env"
}


