
output "secret_manager_id" {
  value = module.secret_manager.secret_manager_id
}

output "secret_manager" {
  value = module.secret_manager.secret_manager
}
