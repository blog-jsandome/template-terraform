terraform {

  required_providers {

    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {

  region     = var.aws_region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key

}

module "secret_manager" {
  source          = "../../../modules/secret_manager"
  app_name        = var.app_name
  app_environment = var.app_environment
  resource        = "secretmanager"

}

module "secret_manager_values" {
  source         = "../../../modules/secret_manager/secret_manager_version"
  secret_id      = module.secret_manager.secret_manager_id
  depends_secret = [module.secret_manager.secret_manager]
  access_keys    = var.access_keys

}
