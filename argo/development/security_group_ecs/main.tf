terraform {

  required_providers {

    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {

  region     = var.aws_region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key

}

data "aws_vpc" "argo" {
  tags = {
    Name = "${var.app_name}-vpc-${var.app_environment}"
  }
}

data "aws_security_group" "sg_alb" {
  vpc_id = data.aws_vpc.argo.id
  tags = {
    Name = "${var.app_name}-sg-alb-development"
  }
}

module "sg_ecs" {
  source              = "../../../modules/security_group_static"
  description_ingress = "Default for sg-alb and sg-ecs"
  app_name            = var.app_name
  app_environment     = var.app_environment
  resource            = "sg-ecs-development"
  project             = "argo"
  id_vpc              = data.aws_vpc.argo.id
  security_groups     = [data.aws_security_group.sg_alb.id]
}

/*
# Ingress rules

module "sg_rule_ingress" {
  source    = "../../../modules/security_group/ingress_rule_sourcesg"
  from_port = 0
  to_port   = 0
  protocol  = "-1"
  source_security_group_id = [
    {
      rule                     = "all"
      source_security_group_id = data.aws_security_group.sg_alb.id
    },
  ]
  sg_id = module.sg_ecs.security_id
}

# Egress rules 

module "sg_rule_default" {
  source = "../../../modules/security_group/egress_rule"
  sg_id  = module.sg_ecs.security_id
}

*/
