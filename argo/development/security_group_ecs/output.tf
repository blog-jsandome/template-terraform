output "id_vpc" {
  value = module.sg_ecs.id_vpc
}

output "security_id" {
  value = module.sg_ecs.security_id
}
