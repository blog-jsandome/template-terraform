
output "ecs_cluster_id" {
  value = module.ecs_cluster.ecs_cluster_id
}

output "cluster_name" {
  value = module.ecs_cluster.cluster_name
}

output "ecs_service_id" {
  value = module.ecs_service.ecs_service_id
}

output "ecs_service_name" {
  value = module.ecs_service.ecs_service_name
}

output "task_family" {
  value = module.ecs_task.task_family
}

output "task_revision" {
  value = module.ecs_task.task_revision
}

output "task_definition" {
  value = module.ecs_task.task_definition
}

output "task_arn" {
  value = module.ecs_task.task_arn
}

output "name_group" {
  value = module.ecs_cloudwatch.name_group
}

output "id_group" {
  value = module.ecs_cloudwatch.id_group
}

output "iam_role_arn" {
  value = module.iam_ecs.iam_role_arn
}


