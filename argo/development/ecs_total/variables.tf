variable "aws_access_key" {
  type        = string
  description = "AWS Access Key"
}

# Provider vars
variable "aws_secret_key" {
  type        = string
  description = "AWS Secret Key"
}

variable "aws_region" {
  type        = string
  description = "AWS Region"
}

variable "app_name" {
  type        = string
  description = "Application Name"
}

variable "app_environment" {
  type        = string
  description = "Application Environment"
}

variable "container_registry" {
  type        = string
  description = "GitLab container_registry "
}

variable "container_registry_website" {
  type        = string
  description = "GitLab container_registry "
}

variable "docker_image" {
  type        = string
  description = "The name and tag docker image from gitlab repository"
}

variable "docker_image_website" {
  type        = string
  description = "The name and tag docker image from gitlab repository"
}

variable "resource_sm" {
  type        = string
  description = "Name resource sm"
}

variable "listener_arn" {
  type        = string
  description = "ARN http listener"
}

variable "name_bucket" {
  type        = string
  description = "Bucket name for variable ecs"
}

variable "arn_tg" {
  type        = string
  description = "resource target group ecs tag"
}

variable "name_tg" {
  type        = string
  description = "name tg"
}

variable "arn_tg_web" {
  type        = string
  description = "resource target group ecs tag"
}

variable "name_tg_web" {
  type        = string
  description = "name tg"
}

variable "sub_project" {
  type        = string
  description = "the name of sub project from the princial project example: BOT"
}

variable "sub_project2" {
  type        = string
  description = "the name of sub project from the princial project example: BOT"
}
