# Get VPC was created before execute this data

data "aws_vpc" "argo" {
  tags = {
    Name = "${var.app_name}-vpc-${var.app_environment}"
  }
}

# Get Public subnets with the tag Tier

data "aws_subnet_ids" "private" {
  vpc_id = data.aws_vpc.argo.id
  tags = {
    Tier = "private"
  }
}

data "aws_subnet" "private_selected" {
  count = length(data.aws_subnet_ids.private.ids)
  id    = tolist(data.aws_subnet_ids.private.ids)[count.index]
}

data "aws_alb_listener" "http_listener" {
  arn = var.listener_arn
}

data "aws_s3_bucket" "ecs_bucket" {
  bucket = var.name_bucket
}

data "aws_alb_target_group" "tg_alb_ecs" {
  arn  = var.arn_tg
  name = var.name_tg
}

data "aws_alb_target_group" "tg_alb_ecs_web" {
  arn  = var.arn_tg_web
  name = var.name_tg_web
}

data "aws_secretsmanager_secret" "secretmanager_ecs" {
  name = "${var.app_name}-deploy-${var.resource_sm}"
}

data "aws_security_group" "sg_alb" {
  tags = {
    Name = "${var.app_name}-sg-alb-development"
  }
}

data "aws_security_group" "sg_ecs" {
  tags = {
    Name = "${var.app_name}-sg-ecs-development"
  }
}
