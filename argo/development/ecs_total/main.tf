terraform {

  required_providers {

    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {

  region     = var.aws_region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key

}

module "ecs_cluster" {
  source          = "../../../modules/ecs-cluster"
  app_name        = var.app_name
  app_environment = var.app_environment
  resource        = "cluster"

}

module "ecs_service" {
  source           = "../../../modules/ecs-service"
  app_name         = var.app_name
  app_environment  = var.app_environment
  target_group_arn = data.aws_alb_target_group.tg_alb_ecs.arn
  cluster_id       = module.ecs_cluster.ecs_cluster_id
  private_subnets  = data.aws_subnet.private_selected.*.id
  security_groups  = [data.aws_security_group.sg_alb.id, data.aws_security_group.sg_ecs.id]
  task_definition  = module.ecs_task.task_arn
  sub_project      = var.sub_project

}

module "ecs_service_website" {
  source           = "../../../modules/ecs-service"
  app_name         = var.app_name
  app_environment  = var.app_environment
  target_group_arn = data.aws_alb_target_group.tg_alb_ecs_web.arn
  cluster_id       = module.ecs_cluster.ecs_cluster_id
  private_subnets  = data.aws_subnet.private_selected.*.id
  security_groups  = [data.aws_security_group.sg_alb.id, data.aws_security_group.sg_ecs.id]
  task_definition  = module.ecs_task_website.task_arn
  container_port   = 801
  sub_project      = var.sub_project2

}

/**module "ecs_service" {
  source           = "../../../modules/ecs-service-no-alb"
  app_name         = var.app_name
  app_environment  = var.app_environment
  target_group_arn = data.aws_alb_target_group.tg_alb_ecs.arn
  cluster_id       = module.ecs_cluster.ecs_cluster_id
  private_subnets  = data.aws_subnet.private_selected.*.id
  security_groups  = [data.aws_security_group.sg_alb.id, data.aws_security_group.sg_ecs.id]
  task_definition  = module.ecs_task.task_arn
  sub_project      = var.sub_project

}**/

module "ecs_cloudwatch" {
  source          = "../../../modules/cloud_watch"
  app_name        = var.app_name
  app_environment = var.app_environment
}

module "ecs_cloudwatch_website" {
  source          = "../../../modules/cloud_watch"
  app_name        = "${var.app_name}-${var.sub_project2}"
  app_environment = var.app_environment
}

module "iam_ecs" {
  source            = "../../../modules/role-iam-ecs"
  project           = "argo"
  resource          = "iam_role_ecs"
  app_name          = var.app_name
  app_environment   = var.app_environment
  secretsmanager_id = data.aws_secretsmanager_secret.secretmanager_ecs.id
  name_bucket       = data.aws_s3_bucket.ecs_bucket.bucket
}

module "ecs_task" {
  source                  = "../../../modules/ecs-task"
  name_bucket             = data.aws_s3_bucket.ecs_bucket.bucket
  project                 = "argo"
  app_name                = var.app_name
  app_environment         = var.app_environment
  resource                = "ecs-task"
  container_registry      = var.container_registry
  docker_image            = var.docker_image
  cpu                     = "1024"
  memory                  = "4096"
  aws_cloudwatch_group_id = module.ecs_cloudwatch.id_group
  aws_region              = var.aws_region
  execution_role_arn      = module.iam_ecs.iam_role_arn
  task_role_arn           = module.iam_ecs.iam_role_arn
  secretsmanager_id       = data.aws_secretsmanager_secret.secretmanager_ecs.id
  sub_project             = var.sub_project
}

module "ecs_task_website" {
  source                  = "../../../modules/ecs-task"
  name_bucket             = data.aws_s3_bucket.ecs_bucket.bucket
  project                 = "argo"
  app_name                = var.app_name
  app_environment         = var.app_environment
  resource                = "ecs-task"
  container_registry      = var.container_registry_website
  docker_image            = var.docker_image_website
  cpu                     = "1024"
  memory                  = "4096"
  aws_cloudwatch_group_id = module.ecs_cloudwatch_website.id_group
  aws_region              = var.aws_region
  execution_role_arn      = module.iam_ecs.iam_role_arn
  task_role_arn           = module.iam_ecs.iam_role_arn
  secretsmanager_id       = data.aws_secretsmanager_secret.secretmanager_ecs.id
  sub_project             = var.sub_project2
  file_vars               = "web.env"
  container_port          = "801"
  host_port               = "801"
}


