terraform {

  required_providers {

    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {

  region     = var.aws_region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key

}

# network configuration

module "network" {
  source             = "../../../modules/networking"
  availability_zones = ["us-east-2a", "us-east-2b"]
  public_subnets     = ["10.0.100.0/24", "10.0.101.0/24"]
  private_subnets    = ["10.0.1.0/24", "10.0.2.0/24"]
  app_name           = var.app_name
  app_environment    = var.app_environment

}

