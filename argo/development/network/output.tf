
output "id_vpc" {
  value = module.network.id_vpc
}

output "id_public_subnets" {
  value = module.network.id_public_subnets
}

output "id_private_subnets" {
  value = module.network.id_private_subnets
}
