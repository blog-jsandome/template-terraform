variable "aws_access_key" {
  type        = string
  description = "AWS Access Key"
}

# Provider vars
variable "aws_secret_key" {
  type        = string
  description = "AWS Secret Key"
}

variable "aws_region" {
  type        = string
  description = "AWS Region"
}

# APP vars

variable "app_name" {
  type        = string
  description = "Application Name"
}


variable "app_environment" {
  type        = string
  description = "Application Environment"
}

# NET vars

variable "cidr" {
  description = "The CIDR block for the VPC."
  default     = "10.0.0.0/16"
}

variable "public_subnets" {
  description = "List of public subnets"
}

variable "private_subnets" {
  description = "List of private subnets"
}

variable "availability_zones" {
  description = "List of availability zones"
}

# ECS vars

#variable "name_bucket" {
#  type        = string
#  description = "bucket name for ecs env"
#}

#variable "image_mkbox" {
#  type        = string
#  description = "docker repo image for mkbox-backend"
#}

#variable "secret_mkbox" {
#  type        = string
#  description = "secret manager for private registry"
#}

# S3 PUBLIC vars

#variable "domain_name" {
#  type        = string
#  description = "name public s3"

#}

# Certificate

#variable "certificate_mkbox" {
#  type        = string
#  description = "certificate *.mktbox.mx"

#}

#variable "certificate_cloudfront" {
#  type        = string
#  description = "certificate from virgina"
#}

#variable "protocol_version" {
#  type        = string
#  description = "Cloudfront TLS version"
#}

