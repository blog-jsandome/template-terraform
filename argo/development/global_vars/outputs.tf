output "region" {
  value = "us-west-1"
}

output "tags" {
  value = {
    environment = "production"
    project     = "campaign_centric"
  }
}
