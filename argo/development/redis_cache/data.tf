
data "aws_vpc" "capa" {
  tags = {
    Name = "${var.app_name}-vpc-${var.app_environment}"
  }
}

# Get Public subnets with the tag Tier

data "aws_subnet_ids" "private" {
  vpc_id = data.aws_vpc.capa.id
  tags = {
    Tier = "private"
  }
}

data "aws_subnet" "private_selected" {
  count = length(data.aws_subnet_ids.private.ids)
  id    = tolist(data.aws_subnet_ids.private.ids)[count.index]
}
