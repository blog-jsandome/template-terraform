
terraform {

  required_providers {

    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {

  region     = var.aws_region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key

}

resource "aws_elasticache_cluster" "redis-cache" {
  cluster_id           = "capa-rediscache-staging"
  engine               = "redis"
  node_type            = "cache.t2small.large"
  num_cache_nodes      = 1
  parameter_group_name = "default.memcached1.4"
  port                 = 6379
}


module "sg_redis" {
  source          = "../../../modules/security_group"
  app_name        = var.app_name
  app_environment = var.app_environment
  resource        = "sg-redis-staging"
  project         = "capa"
  id_vpc          = data.aws_vpc.capa.id
}

# Ingress rules
module "sg_rule_ingress_redis" {
  source           = "../../../modules/security_group/ingress_rule"
  from_port        = 6379
  to_port          = 6379
  protocol         = "tcp"
  cidr_blocks      = ["10.0.0.0/16"]
  ipv6_cidr_blocks = ["::/0"]
  sg_id            = module.sg_redis.security_id
}


# Egress rules 

module "sg_rule_default_redis" {
  source = "../../../modules/security_group/egress_rule"
  sg_id  = module.sg_redis.security_id
}

module "redis_cache" {
  app_name             = var.app_name
  app_environment      = var.app_environment
  name_subnetgroup     = "sg-capa-redis-staging"
  source               = "../../../modules/elastic_cache"
  cluster_id           = "capa-rediscache-staging"
  engine               = "redis"
  engine_version       = "6.x"
  node_type            = "cache.t2.small"
  num_cache_nodes      = 1
  parameter_group_name = ""
  port                 = 6379
  subnets_id           = ["${data.aws_subnet.private_selected.0.id}", "${data.aws_subnet.private_selected.1.id}"]
  security_groups      = [module.sg_redis.security_id]

}

