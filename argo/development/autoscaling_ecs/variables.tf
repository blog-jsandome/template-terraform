variable "aws_access_key" {
  type        = string
  description = "AWS Access Key"
}

# Provider vars
variable "aws_secret_key" {
  type        = string
  description = "AWS Secret Key"
}

variable "aws_region" {
  type        = string
  description = "AWS Region"
}

variable "app_name" {
  type        = string
  description = "Application Name"
}

variable "app_environment" {
  type        = string
  description = "Application Environment"
}

variable "ecs_cluster_name" {
  type        = string
  description = "Name of ecs cluster"
}

variable "ecs_service_name_s" {
  type        = string
  description = "Name of ecs service"
}

variable "ecs_service_name_w" {
  type        = string
  description = "Name of ecs service"
}

variable "sub_project1" {
  type        = string
  description = "variable for subproject name"
}

variable "sub_project2" {
  type        = string
  description = "variable for subproject name"
}
