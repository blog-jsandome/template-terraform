terraform {

  required_providers {

    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {

  region     = var.aws_region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key

}

module "autoscaling_ecs_strapi" {
  source               = "../../../modules/autoscaling_ecs"
  app_name             = var.app_name
  app_environment      = var.app_environment
  aws_ecs_cluster_name = var.ecs_cluster_name
  aws_ecs_service_name = var.ecs_service_name_s
  sub_project          = var.sub_project1
}

module "autoscaling_ecs_web" {
  source               = "../../../modules/autoscaling_ecs"
  app_name             = var.app_name
  app_environment      = var.app_environment
  aws_ecs_cluster_name = var.ecs_cluster_name
  aws_ecs_service_name = var.ecs_service_name_w
  sub_project          = var.sub_project2
}

