# Get VPC was created before execute this data

data "aws_vpc" "amgen" {
  tags = {
    Name = "${var.app_name}-vpc-${var.app_environment}"
  }
}

# Get Public subnets with the tag Tier

data "aws_subnet_ids" "private" {
  vpc_id = data.aws_vpc.amgen.id
  tags = {
    Tier = "private"
  }
}

data "aws_subnet_ids" "public" {
  vpc_id = data.aws_vpc.amgen.id
  tags = {
    Tier = "public"
  }
}

data "aws_subnet" "private_selected" {
  count = length(data.aws_subnet_ids.private.ids)
  id    = tolist(data.aws_subnet_ids.private.ids)[count.index]
}

data "aws_subnet" "public_selected" {
  count = length(data.aws_subnet_ids.public.ids)
  id    = tolist(data.aws_subnet_ids.public.ids)[count.index]
}


