terraform {

  required_providers {

    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {

  region     = var.aws_region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key

}

module "iam_role_eks" {
  source          = "../../../modules/role-iam-eks"
  app_name        = var.app_name
  app_environment = var.app_environment
  project         = "sagemaker"
  resource        = "iam-role"

}

module "iam_role_eks_group" {
  source          = "../../../modules/role-iam-eks"
  app_name        = var.app_name
  app_environment = var.app_environment
  project         = "sagemaker"
  resource        = "iam-role-group"

}

module "eks_cluster" {
  source          = "../../../modules/eks"
  app_name        = var.app_name
  app_environment = var.app_environment
  resource        = "cluster-eks"
  role_arn        = module.iam_role_eks.iam_role_arn
  subnet_ids      = data.aws_subnet.public_selected.*.id
}


module "eks_nodegroup" {
  source          = "../../../modules/eks-nodes"
  app_name        = var.app_name
  app_environment = var.app_environment
  resource        = "nodegroup-eks"
  node_role_arn   = module.iam_role_eks_group.iam_role_arn
  subnet_ids      = data.aws_subnet.private_selected.*.id
  capacity_type   = "ON_DEMAND"
  instance_types  = ["t3.small"]
  desired_size    = 1
  max_size        = 5
  min_size        = 0
  max_unavailable = 1
  role            = "general"
  cluster_name    = module.eks_cluster.cluster_name

}
