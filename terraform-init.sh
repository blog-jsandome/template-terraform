#!/bin/bash

# personal access token glpat-CD-TmSseszvoBRKyRjVD

# network

export GITLAB_ACCESS_TOKEN=glpat-CD-TmSseszvoBRKyRjVD
export TF_STATE_NAME=network
terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/38377171/terraform/state/$TF_STATE_NAME" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/38377171/terraform/state/$TF_STATE_NAME/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/38377171/terraform/state/$TF_STATE_NAME/lock" \
    -backend-config="username=jsandome" \
    -backend-config="password=$GITLAB_ACCESS_TOKEN" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"


# eks cluster 

export GITLAB_ACCESS_TOKEN=glpat-CD-TmSseszvoBRKyRjVD
export TF_STATE_NAME=eks
terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/38377171/terraform/state/$TF_STATE_NAME" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/38377171/terraform/state/$TF_STATE_NAME/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/38377171/terraform/state/$TF_STATE_NAME/lock" \
    -backend-config="username=jsandome" \
    -backend-config="password=$GITLAB_ACCESS_TOKEN" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"