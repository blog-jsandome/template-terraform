
resource "aws_ecs_cluster" "aws-ecs-cluster" {
  name = "${var.app_name}-${var.app_environment}-${var.resource}"
  tags = {
    Name        = "${var.app_name}-${var.resource}"
    Environment = var.app_environment
    Resource    = var.resource
    Project     = var.app_name
  }
}
