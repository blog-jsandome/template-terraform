
variable "app_name" {
  type        = string
  description = "name app"
}

variable "app_environment" {
  type        = string
  description = "Environment"
}

variable "resource" {
  type        = string
  description = "Resource or service to use"
}

variable "port" {
  type        = number
  description = "Default port 80"
  default     = 80
}

variable "protocol" {
  type        = string
  description = "Default HTTP"
  default     = "HTTP"
}

variable "id_vpc" {
  type        = string
  description = "VPC id, it's required"
}

variable "target_type" {
  type        = string
  description = "Default target ip"
  default     = "ip"
}

variable "healthy" {
  type        = string
  description = "Check the times for the healthy"
  default     = "3"
}

variable "interval" {
  type        = string
  description = "seconds interval"
  default     = "300"
}

variable "matcher" {
  type        = string
  description = "matcher default 200"
  default     = "200"
}

variable "timeout" {
  type        = string
  description = "Timeout default 3 "
  default     = "3"
}

variable "path" {
  type        = string
  description = "Path for healthy"
  default     = "/v1/status"
}

variable "unhealthy" {
  type        = string
  description = "times for unhealthy"
  default     = "2"
}
