

resource "aws_alb_target_group" "target_group_default" {
  name        = "${var.app_name}-${var.resource}"
  port        = var.port
  protocol    = var.protocol
  vpc_id      = var.id_vpc
  target_type = var.target_type
  health_check {
    healthy_threshold   = var.healthy
    interval            = var.interval
    protocol            = var.protocol
    matcher             = var.matcher
    timeout             = var.timeout
    path                = var.path
    unhealthy_threshold = var.unhealthy
  }
  tags = {
    Name        = "${var.app_name}-${var.resource}"
    Environment = var.app_environment
    Resource    = var.resource
  }
}
