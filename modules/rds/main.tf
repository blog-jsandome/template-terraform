resource "aws_db_subnet_group" "subn-groups" {
  subnet_ids = var.subnets_id
}

resource "aws_db_instance" "rds_database" {
  name                      = var.name
  identifier                = var.identifier
  instance_class            = var.instance_class
  username                  = var.username
  password                  = var.password
  engine                    = var.engine
  engine_version            = var.engine_version
  allocated_storage         = var.allocated_storage
  storage_type              = var.storage_type
  multi_az                  = var.multi_az
  db_subnet_group_name      = aws_db_subnet_group.subn-groups.name
  vpc_security_group_ids    = var.security_group_id
  kms_key_id                = var.kms_key_id
  publicly_accessible       = var.publicy_accessible
  skip_final_snapshot       = var.skip_final_snapshot
  backup_retention_period   = var.backup_retention_period
  port                      = var.port
  parameter_group_name      = var.parameter_group_name
  final_snapshot_identifier = var.final_snapshot_identifier
  tags = {
    Name        = var.app_name
    Environment = var.app_environment
  }
}

