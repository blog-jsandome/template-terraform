
variable "app_name" {
  type        = string
  description = "app name"
}

variable "app_environment" {
  type        = string
  description = "Environment"
}

variable "subnets_id" {
  type        = list(string)
  description = "list of the private or public subnets"
  default     = []
}

variable "name" {
  type        = string
  description = "name of the db"
}

variable "identifier" {
  type        = string
  description = "identifier name rds"
}

variable "instance_class" {
  type        = string
  description = "class or family instances"
}

variable "username" {
  type        = string
  description = "Username for the RDS"
}

variable "password" {
  type        = string
  description = "password for the username root"
  sensitive   = true
}

variable "engine" {
  type        = string
  description = "engine of RDS"
}

variable "engine_version" {
  type        = string
  description = "engine version"
}

variable "allocated_storage" {
  type        = number
  description = "allocated storage "
  default     = 10
}

variable "storage_type" {
  type        = string
  description = "default gp2"
  default     = "gp2"
}

variable "multi_az" {
  description = "multi_az default false"
  default     = false
}

variable "publicly_accessible" {
  description = "public access"
  default     = false
}

variable "security_group_id" {
  type        = list(any)
  description = "The security group created for the RDS"
}

variable "kms_key_id" {
  type        = string
  description = "IF you created a kms key"
  default     = ""
}

variable "publicy_accessible" {
  description = "default access false"
  default     = false
}

variable "skip_final_snapshot" {
  description = "skip final default false"
  default     = true
}

variable "backup_retention_period" {
  description = "retetion of backups"
  default     = 7
}

variable "port" {
  type        = number
  description = "the port that the engine needs"
}

variable "parameter_group_name" {
  type        = string
  description = "Parameter group name"
}

variable "final_snapshot_identifier" {
  description = "final_snap"
  default     = false
}
