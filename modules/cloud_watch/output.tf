output "name_group" {
  value = aws_cloudwatch_log_group.log-group.name
}

output "id_group" {
  value = aws_cloudwatch_log_group.log-group.id
}
