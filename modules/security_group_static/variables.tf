# APP vars
variable "app_name" {
  type        = string
  description = "Application Name"
}

variable "app_environment" {
  type        = string
  description = "Application Environment"
}

variable "resource" {
  type        = string
  description = "resource for example ALB, ECS, EC2"
}

variable "project" {
  type        = string
  description = "Name project"
}

# SG
variable "id_vpc" {
  type        = string
  description = "ID VPC"
}

variable "description" {
  type        = string
  description = "desc for sg"
  default     = "AWS Security group"
}

variable "description_ingress" {
  type        = string
  description = "ID VPC"
}

variable "from_port_ingress" {
  type        = number
  description = "Default port 0"
  default     = 0
}

variable "to_port_ingress" {
  type        = number
  description = "Default port to 0"
  default     = 0
}

variable "protocol_ingress" {
  type        = string
  description = "Default -1"
  default     = "-1"
}



variable "security_groups" {
  type        = list(any)
  description = "list security_groups"
  default     = []
}

variable "from_port_egress" {
  type        = number
  description = "Default port 0"
  default     = 0
}

variable "to_port_egress" {
  type        = number
  description = "Default port to 0"
  default     = 0
}

variable "protocol_egress" {
  type        = string
  description = "Default -1"
  default     = "-1"
}


variable "cidr_blocks_egress" {
  type        = list(any)
  description = "Default cidr range"
  default     = ["0.0.0.0/0"]
}

variable "ipv6_cidr_blocks_egress" {
  type        = list(any)
  description = "Default ipv6 range"
  default     = ["::/0"]
}


