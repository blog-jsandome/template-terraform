

resource "aws_security_group" "aws-sg" {
  name        = "${var.app_name}-${var.resource}"
  description = var.description
  vpc_id      = var.id_vpc

  ingress {
    description     = var.description_ingress
    from_port       = var.from_port_ingress
    to_port         = var.to_port_ingress
    protocol        = var.protocol_ingress
    security_groups = var.security_groups
  }

  egress {
    from_port        = var.from_port_egress
    to_port          = var.to_port_egress
    protocol         = var.protocol_egress
    cidr_blocks      = var.cidr_blocks_egress
    ipv6_cidr_blocks = var.ipv6_cidr_blocks_egress
  }

  tags = {
    Name        = "${var.app_name}-${var.resource}"
    Environment = var.app_environment
    Project     = var.project
    Resource    = var.resource
  }
}

