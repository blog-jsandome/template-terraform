
variable "app_name" {
  type        = string
  description = "Application Name"
}

variable "app_environment" {
  type        = string
  description = "Application Environment"
}

variable "cluster_id" {
  type        = string
  description = "ID cluster ecs"
}


variable "task_definition" {
  type        = string
  description = "all de componentes  "
}

variable "launch_type" {
  type        = string
  description = "Type service, default FARGATE"
  default     = "FARGATE"
}

variable "strategy" {
  type        = string
  description = "Type strategy, default REPLICA"
  default     = "REPLICA"
}

variable "desired_count" {
  type        = number
  description = "number of service, default 1"
  default     = 1
}

variable "force_new_deployment" {
  description = "Force enable"
  default     = true
}

variable "private_subnets" {
  type        = list(any)
  description = "List of the private subnets"
}

variable "assign_public_ip" {
  description = "Disable public ip for security"
  default     = false
}

variable "security_groups" {
  description = "The security groups alb and ecs"
  type        = list(string)
  default     = []
}

variable "target_group_arn" {
  type        = string
  description = "ID target group"
}

variable "container_port" {
  type        = number
  description = "Port for the container, default 80"
  default     = 80
}

variable "sub_project" {
  type        = string
  description = "the name of sub project from the princial project example: ARGO_BOT"
}

#variable "alb_listener_http" {
#  type        = list(any)
#  description = "Dependency for to create alb in service"
#}
