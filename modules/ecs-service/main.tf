resource "aws_ecs_service" "aws-ecs-service" {
  name                 = "${var.app_name}-${var.app_environment}-ecs-service-${var.sub_project}"
  cluster              = var.cluster_id
  task_definition      = var.task_definition
  launch_type          = var.launch_type
  scheduling_strategy  = var.strategy
  desired_count        = var.desired_count
  force_new_deployment = var.force_new_deployment

  network_configuration {
    subnets          = var.private_subnets
    assign_public_ip = var.assign_public_ip
    security_groups  = var.security_groups
  }

  load_balancer {
    target_group_arn = var.target_group_arn
    container_name   = "${var.app_name}-${var.app_environment}-container-${var.sub_project}"
    container_port   = var.container_port
  }
}
