
output "secret_manager_id" {
  value = aws_secretsmanager_secret.secret_manager.id
}

output "secret_manager" {
  value = aws_secretsmanager_secret.secret_manager
}
