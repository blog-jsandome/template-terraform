
resource "aws_secretsmanager_secret" "secret_manager" {
  name = "${var.app_name}-deploy-${var.resource}"

  tags = {
    Name        = "${var.app_name}-${var.resource}"
    Environment = var.app_environment
    Resource    = var.resource
    Project     = var.app_name
  }

}





