
resource "aws_secretsmanager_secret_version" "ecs_cluster" {
  secret_id     = var.secret_id
  secret_string = jsonencode(var.access_keys)

  depends_on = [var.depends_secret]
}
