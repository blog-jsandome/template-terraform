
variable "secret_id" {
  type        = string
  description = "ID Secret Manager it's required"
}

variable "access_keys" {
  type        = map(string)
  description = "Access key"
  sensitive   = true
  default = {
    username = "username"
    password = "password"
  }
}

variable "depends_secret" {
  type        = list(any)
  description = "Secret Manager was created"
}

