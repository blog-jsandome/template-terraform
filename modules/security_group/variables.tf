# APP vars
variable "app_name" {
  type        = string
  description = "Application Name"
}

variable "app_environment" {
  type        = string
  description = "Application Environment"
}

variable "resource" {
  type        = string
  description = "resource for example ALB, ECS, EC2"
}

variable "project" {
  type        = string
  description = "Name project"
}

# SG
variable "id_vpc" {
  type        = string
  description = "ID VPC"
}

variable "description" {
  type        = string
  description = "desc for sg"
  default     = "AWS Security group"
}


