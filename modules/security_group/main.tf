

resource "aws_security_group" "aws-sg" {
  name        = "${var.app_name}-${var.resource}"
  description = var.description
  vpc_id      = var.id_vpc
  tags = {
    Name        = "${var.app_name}-${var.resource}"
    Environment = var.app_environment
    Project     = var.project
    Resource    = var.resource
  }
}
