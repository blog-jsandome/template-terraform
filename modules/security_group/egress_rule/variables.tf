
variable "type" {
  type        = string
  description = "Default type ingress"
  default     = "egress"
}

variable "from_port" {
  type        = number
  description = "from port, it's required"
  default     = 0
}

variable "to_port" {
  type        = number
  description = "to port, it's required"
  default     = 0
}

variable "protocol" {
  type        = string
  description = "protocol tcp, upd, http, https if you select a protocol of -1, you mush specify a from and to port equal to 0. it's required"
  default     = "-1"
}

variable "cidr_blocks" {
  type        = list(any)
  description = " list of CIDR blocks. it's required"
  default     = ["0.0.0.0/0"]
}

variable "ipv6_cidr_blocks" {
  type        = list(any)
  description = "list of CIDR IPV6 blocks"
  default     = ["::/0"]
}

variable "sg_id" {
  type        = string
  description = "Security id. it's required"
}
