

resource "aws_security_group_rule" "ingress_rule" {
  type                     = var.type
  from_port                = var.from_port
  to_port                  = var.to_port
  protocol                 = var.protocol
  source_security_group_id = var.source_security_group_id
  security_group_id        = var.sg_id
}



resource "aws_security_group_rule" "aws_security_group_rule_suffix" {
  type                     = "Required"
  cidr_blocks              = "Optional"
  ipv6_cidr_blocks         = "Optional"
  prefix_list_ids          = "Optional"
  from_port                = "Required"
  protocol                 = "Required"
  security_group_id        = "Required"
  source_security_group_id = "Optional"
  self                     = "Optional"
  to_port                  = "Required"
  description              = "Optional"
}
