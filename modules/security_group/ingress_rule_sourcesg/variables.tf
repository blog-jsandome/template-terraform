
variable "type" {
  type        = string
  description = "Default type ingress"
  default     = "ingress"
}

variable "from_port" {
  type        = number
  description = "from port, it's required"
}

variable "to_port" {
  type        = number
  description = "to port, it's required"
}

variable "protocol" {
  type        = string
  description = "protocol tcp, upd, http, https if you select a protocol of -1, you mush specify a from and to port equal to 0. it's required"
}

variable "source_security_group_id" {
  type        = list(map(string))
  description = "List of security groups. Use this variable instead of cidr_blocks "
  default     = []

}

variable "sg_id" {
  type        = string
  description = "Security id. it's required"
}
