resource "aws_lb_listener_rule" "host_based_weighted_routing" {
  listener_arn = var.listener_arn
  priority     = var.priority

  action {
    type             = var.type
    target_group_arn = var.target_group_arn
  }

  condition {
    host_header {
      values = [var.host_header]
    }
  }
}
