
variable "listener_arn" {
  type        = string
  description = "arn listener"
}

variable "priority" {
  type        = number
  description = "priority"
  default     = 10
}

variable "type" {
  type        = string
  description = "type listener"
  default     = "forward"
}

variable "target_group_arn" {
  type        = string
  description = "target group"
}

variable "host_header" {
  type        = string
  description = "host headear or domain like www.test.com"
}
