output "listener_arn_rule" {
  value = aws_lb_listener_rule.host_based_weighted_routing.arn
}
