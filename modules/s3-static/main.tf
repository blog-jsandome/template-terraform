resource "aws_s3_bucket" "website_bucket" {
  bucket = var.domain_name
  acl    = var.acl
  policy = data.aws_iam_policy_document.website_policy.json
  versioning {
    enabled = var.versioning
  }
  website {
    index_document = var.index_doc
    error_document = var.error_doc
  }
}

data "aws_iam_policy_document" "website_policy" {
  statement {
    actions = [
      "s3:GetObject"
    ]
    principals {
      identifiers = ["*"]
      type        = "AWS"
    }
    resources = [
      "arn:aws:s3:::${var.domain_name}/*"
    ]
  }
}
