
variable "domain_name" {
  type        = string
  description = " Bucket's name"
}

variable "acl" {
  type        = string
  description = "ACL private or public"
  default     = "public-read"
}

variable "versioning" {
  description = "boolean for disable or enable the versioning"
  default     = true
}

variable "index_doc" {
  type        = string
  description = "index file"
  default     = "index.html"
}

variable "error_doc" {
  type        = string
  description = "error file"
  default     = "index.html"
}
