
output "task_family" {
  value = aws_ecs_task_definition.aws-ecs-task.family
}

output "task_revision" {
  value = aws_ecs_task_definition.aws-ecs-task.revision
}


output "task_definition" {
  value = aws_ecs_task_definition.aws-ecs-task.family
}

output "task_arn" {
  value = aws_ecs_task_definition.aws-ecs-task.arn
}
