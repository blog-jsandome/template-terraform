
resource "aws_ecs_task_definition" "aws-ecs-task" {
  family = "${var.app_name}-task-${var.sub_project}"

  container_definitions = <<DEFINITION
  [
    {
      "name": "${var.app_name}-${var.app_environment}-container-${var.sub_project}",
      "image": "${var.container_registry}${var.docker_image}",
      "entryPoint": [],
      "environmentFiles": [{"Type": "s3" , "Value": "arn:aws:s3:::${var.name_bucket}/${var.file_vars}"}],
      "repositoryCredentials": {
        "credentialsParameter": "${var.secretsmanager_id}"
      },
      "essential": true,
      "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-group": "${var.aws_cloudwatch_group_id}",
          "awslogs-region": "${var.aws_region}",
          "awslogs-stream-prefix": "${var.app_name}-${var.app_environment}"
        }
      },
      "portMappings": [
        {
          "containerPort": ${var.container_port},
          "hostPort": ${var.host_port}
        }
      ],
      "cpu": ${var.cpu},
      "memory": ${var.memory},
      "networkMode": "${var.network_mode}"
    }
  ]
  DEFINITION

  requires_compatibilities = var.requires_compatibilities
  network_mode             = var.network_mode
  memory                   = var.memory
  cpu                      = var.cpu
  execution_role_arn       = var.execution_role_arn
  task_role_arn            = var.task_role_arn

  tags = {
    Name        = "${var.app_name}-ecs-td"
    Environment = var.app_environment
    Project     = var.project
    Resource    = var.resource
  }
}
