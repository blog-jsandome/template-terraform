
variable "app_name" {
  type        = string
  description = "Application Name"
}

variable "app_environment" {
  type        = string
  description = "Application Environment"
}

variable "resource" {
  type        = string
  description = "resource for example ALB, ECS, EC2"
}

variable "project" {
  type        = string
  description = "Name project"
}

# DEFINITION

variable "container_registry" {
  type        = string
  description = "container_registry GitLab"
}

variable "docker_image" {
  type        = string
  description = "Name and tag docker image"
}

variable "name_bucket" {
  type        = string
  description = "Bucket with the env file"
}

variable "secretsmanager_id" {
  type        = string
  description = "Secret manager for the connection with the registry"
}

variable "aws_cloudwatch_group_id" {
  type        = string
  description = "cloudwatch for logs"
}
variable "aws_region" {
  type        = string
  description = "region for the logs"
}

variable "container_port" {
  type        = string
  description = "port for the container"
  default     = "80"
}

variable "host_port" {
  type        = string
  description = "host port"
  default     = "80"
}

variable "cpu" {
  type        = string
  description = "CPU for the container"
}

variable "memory" {
  type        = string
  description = "RAM for the container"
}

variable "network_mode" {
  type        = string
  description = "for fargate awsvpc"
  default     = "awsvpc"
}

variable "requires_compatibilities" {
  type        = list(string)
  description = "Compatibility, default FARGATE"
  default     = ["FARGATE"]
}

variable "execution_role_arn" {
  type        = string
  description = "IAM role execution"
}

variable "task_role_arn" {
  type        = string
  description = "IAM role task"
}

variable "sub_project" {
  type        = string
  description = "the name of sub project from the princial project example: BOT"
}

variable "file_vars" {
  type        = string
  description = "name of the variables file for the task"
  default     = ".env"
}


