
resource "aws_elasticache_subnet_group" "subnet-group" {
  name       = var.name_subnetgroup
  subnet_ids = var.subnets_id
}


resource "aws_elasticache_cluster" "redis-cache" {
  cluster_id           = var.cluster_id
  engine               = var.engine
  engine_version       = var.engine_version
  node_type            = var.node_type
  num_cache_nodes      = var.num_cache_nodes
  parameter_group_name = var.parameter_group_name
  port                 = var.port
  subnet_group_name    = aws_elasticache_subnet_group.subnet-group.name
  security_group_ids   = var.security_groups
  tags = {
    Name        = var.app_name
    Environment = var.app_environment
  }
}
