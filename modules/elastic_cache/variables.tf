
variable "app_name" {
  type        = string
  description = "app name"
}

variable "app_environment" {
  type        = string
  description = "app Environment"
}

variable "name_subnetgroup" {
  type        = string
  description = "name subnetgroup"
}

variable "subnets_id" {
  type        = list(string)
  description = "subnets id"
}

variable "cluster_id" {
  type        = string
  description = "Name cluster"
}
variable "engine" {
  type        = string
  description = "engine memcache or redis"
}

variable "engine_version" {
  type        = string
  description = "engine version"
}

variable "node_type" {
  type        = string
  description = "node type family"
}

variable "num_cache_nodes" {
  type        = number
  description = "num nodes"
  default     = 1

}

variable "parameter_group_name" {
  type        = string
  description = "parameter version"
  default     = ""
}

variable "port" {
  type        = number
  description = "port number"
}

variable "security_groups" {
  type        = list(any)
  description = "security group id"

}

