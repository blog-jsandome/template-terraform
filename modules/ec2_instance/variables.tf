variable "app_name" {
  type        = string
  description = "Application Name"
}

variable "app_environment" {
  type        = string
  description = "Application Environment"
}

variable "resource" {
  type        = string
  description = "resource"
}

variable "ami" {
  type        = string
  description = "ID AMI, better get this with a data"
}

variable "instance_type" {
  type        = string
  description = "Family instance"
}
