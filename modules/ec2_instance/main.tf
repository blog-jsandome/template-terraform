
resource "aws_instance" "aws-ec2-instance" {
  ami           = var.ami # us-west-2
  instance_type = var.instance_type

  network_interface {
    network_interface_id = var.network_interface_id
    device_index         = var.device_index
  }

  tags = {
    Name        = "${var.app_name}-${var.resource}"
    Environment = var.app_environment
    Resource    = var.resource
    Project     = var.app_name
  }

}
