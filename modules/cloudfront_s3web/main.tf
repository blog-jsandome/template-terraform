
data "aws_s3_bucket" "s3_web" {
  bucket = var.bucket_name

}

resource "aws_cloudfront_distribution" "s3_distribution" {
  origin {
    domain_name = data.aws_s3_bucket.s3_web.bucket_domain_name
    origin_id   = data.aws_s3_bucket.s3_web.id

    custom_origin_config {
      http_port              = var.http_port
      https_port             = var.https_port
      origin_protocol_policy = var.origin_protocol_policy
      origin_ssl_protocols   = [var.origin_ssl_protocols]
    }

  }

  enabled             = var.enabled
  is_ipv6_enabled     = var.is_ipv6_enabled
  comment             = var.comment
  default_root_object = var.default_root_object

  aliases = var.aliases

  default_cache_behavior {
    allowed_methods  = var.allowed_methods
    cached_methods   = var.cached_methods
    target_origin_id = data.aws_s3_bucket.s3_web.id

    forwarded_values {
      query_string = var.query_string

      cookies {
        forward = var.cookies_forward
      }
    }

    min_ttl                = var.min_ttl
    default_ttl            = var.default_ttl
    max_ttl                = var.max_ttl
    viewer_protocol_policy = var.viewer_protocol_policy
  }

  # DEFAULT CUSTOMS ERROR 

  custom_error_response {
    error_caching_min_ttl = 0
    error_code            = 404
    response_code         = 200
    response_page_path    = "/index.html"
  }

  custom_error_response {
    error_caching_min_ttl = 0
    error_code            = 403
    response_code         = 200
    response_page_path    = "/index.html"
  }

  price_class = "PriceClass_200"

  restrictions {
    geo_restriction {
      restriction_type = "none"
      locations        = []
    }
  }

  tags = {
    Application = var.app_name
    Environment = var.app_environment
    Project     = var.project
  }

  viewer_certificate {

    acm_certificate_arn            = var.certificate_cloudfront
    ssl_support_method             = var.certificate_cloudfront == "" ? null : "sni-only"
    minimum_protocol_version       = var.protocol_version
    cloudfront_default_certificate = var.certificate_cloudfront == "" ? true : false
  }
}
