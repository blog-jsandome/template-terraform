variable "app_name" {
  type        = string
  description = "Application Name"
}

variable "app_environment" {
  type        = string
  description = "Application Environment"
}

variable "project" {
  type        = string
  description = "Project name"
}

variable "bucket_name" {
  type        = string
  description = "name bucket"
}

variable "http_port" {
  type        = number
  description = "http port"
  default     = 80
}

variable "https_port" {
  type        = number
  description = "https port"
  default     = 443
}

variable "origin_protocol_policy" {
  type        = string
  description = "origin s3 protocol"
  default     = "match-viewer"
}

variable "origin_ssl_protocols" {
  type        = string
  description = "origin protocol s3 ssl"
  default     = "TLSv1.2"
}

variable "enabled" {
  description = "enable to aceppt end user request"
  default     = true

}

variable "http_version" {
  type        = string
  description = "version http 1 or 2, default 2"
  default     = "http2"
}

variable "is_ipv6_enabled" {
  description = "IPV6 enable"
  default     = true
}

variable "certificate_cloudfront" {
  type        = string
  description = "Certificate ssl"
}

variable "protocol_version" {
  type        = string
  description = "Protocol version"
  default     = "TLSv1.2_2021"
}

variable "aliases" {
  type        = list(string)
  description = "Extra CNAMEs for a domain"
  default     = []
}

variable "comment" {
  type        = string
  description = "comment in cloudfront "
  default     = "cloudfront terraform"
}

variable "default_root_object" {
  type        = string
  description = "default object"
  default     = "index.html"
}

variable "allowed_methods" {
  type        = list(string)
  description = "allow methods"
  default     = ["GET", "HEAD"]
}

variable "cached_methods" {
  type        = list(string)
  description = "cache methods"
  default     = ["GET", "HEAD"]
}

variable "query_string" {
  description = "queries"
  default     = false
}

variable "cookies_forward" {
  type        = string
  description = "cookies"
  default     = "none"
}

variable "min_ttl" {
  type        = number
  description = "min ttl"
  default     = 0
}

variable "default_ttl" {
  type        = number
  description = "default ttl"
  default     = 86400
}

variable "max_ttl" {
  type        = number
  description = "max ttl"
  default     = 31536000
}

variable "viewer_protocol_policy" {
  type        = string
  description = "protocol policy"
}

