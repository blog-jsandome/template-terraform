
variable "app_name" {
  type        = string
  description = "Application Name"
}

variable "app_environment" {
  type        = string
  description = "Application Environment"
}

variable "subnet_ids" {
  type        = list(any)
  description = "List of the private subnets"
}

variable "resource" {
  type        = string
  description = "resource"
}

variable "role_arn" {
  type        = string
  description = "role arn "
}

#variable "alb_listener_http" {
#  type        = list(any)
#  description = "Dependency for to create alb in service"
#}
