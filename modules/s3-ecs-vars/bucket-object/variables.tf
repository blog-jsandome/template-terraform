
variable "bucket_id" {
  type        = string
  description = "ID for the S3 "
}

variable "key" {
  type        = string
  description = "The key is for variables so for default is .var"
  default     = ".env"
}

variable "source_file" {
  type        = string
  description = "Source for the variables file example -> path/to/file.env"
}

variable "content_type" {
  type        = string
  description = "Type in S3"
  default     = "env"
}


