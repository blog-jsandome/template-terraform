resource "aws_s3_bucket_object" "env" {
  bucket       = var.bucket_id
  key          = var.key
  source       = var.source_file
  content_type = var.content_type
}
