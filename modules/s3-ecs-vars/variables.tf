
variable "name_bucket" {
  type        = string
  description = "Name bucket for variables ECS"
}

variable "acl" {
  type        = string
  description = "ACL private or public"
  default     = "private"
}

variable "versioning" {
  description = "boolean for disable or enable the versioning"
  default     = true
}

