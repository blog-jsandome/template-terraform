resource "aws_network_interface" "foo" {
  subnet_id   = var.subnets_id
  private_ips = [var.private_ips]

  tags = {
    Name = "primary_network_interface"
  }
}
