
variable "app_name" {
  type        = string
  description = "Application Name"
}

variable "app_environment" {
  type        = string
  description = "Application Environment"
}

variable "aws_ecs_cluster_name" {
  type        = string
  description = "Name cluster ecs"
}

variable "aws_ecs_service_name" {
  type        = string
  description = "Name service ecs"
}

variable "service_namespace" {
  type        = string
  description = "Name namespace"
  default     = "ecs"
}

variable "sub_project" {
  type        = string
  description = "subproject"
}

variable "target_value" {
  type        = number
  description = "porcent cpu used"
  default     = 80
}
