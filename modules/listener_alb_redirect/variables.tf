

variable "alb_arn" {
  type        = string
  description = "The arn alb"
}

variable "port" {
  type        = string
  description = "The port listen"
}

variable "protocol" {
  type        = string
  description = "protocol for the port"
}

variable "type_action" {
  type        = string
  description = "redirect to https"
  default     = "redirect"
}


variable "port_redirect" {
  type        = string
  description = "port for target redirect"
  default     = "443"
}


variable "protocol_redirect" {
  type        = string
  description = "protocol for redirect"
  default     = "HTTPS"
}


variable "status_code" {
  type        = string
  description = "status code for redirect"
  default     = "HTTP_301"
}
