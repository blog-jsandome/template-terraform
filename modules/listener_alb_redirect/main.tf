

resource "aws_lb_listener" "listener_alb_redirect" {
  load_balancer_arn = var.alb_arn
  port              = var.port
  protocol          = var.protocol

  default_action {
    type = var.type_action



    redirect {
      port        = var.port_redirect
      protocol    = var.protocol_redirect
      status_code = var.status_code
    }

  }
}
