output "role_name" {
  value = aws_iam_role.ecsTaskExecutionRole.name
}

output "iam_role_arn" {
  value = aws_iam_role.ecsTaskExecutionRole.arn
}
