
variable "app_name" {
  type        = string
  description = "Application Name"
}

variable "app_environment" {
  type        = string
  description = "Application Environment"
}

variable "desired_size" {
  type        = number
  description = "size "
}

variable "max_size" {
  type        = number
  description = "max size"
}

variable "min_size" {
  type        = string
  description = "size "
}

variable "capacity_type" {
  type        = string
  description = "type capacity"
}

variable "instance_types" {
  type        = list(any)
  description = "type compute"
}

variable "subnet_ids" {
  type        = list(any)
  description = "subnets ID"
}

variable "max_unavailable" {
  type        = number
  description = "max computer"
}

variable "node_role_arn" {
  type        = string
  description = "role arn"
}

variable "role" {
  type        = string
  description = "role type tag"
}

variable "resource" {
  type        = string
  description = "resource"
}
variable "cluster_name" {
  type        = string
  description = "cluster name"
}
