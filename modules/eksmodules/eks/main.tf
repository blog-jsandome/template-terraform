resource "aws_eks_cluster" "example" {
  name     = "${var.app_name}-${var.app_environment}-${var.resource}"
  role_arn = var.role_arn

  vpc_config {
    subnet_ids = var.subnet_ids
  }

}
