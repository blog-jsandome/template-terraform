output "role_name" {
  value = aws_iam_role.eks_role.name
}

output "iam_role_arn" {
  value = aws_iam_role.eks_role.arn
}
