output "id_vpc" {
  value = aws_vpc.aws-vpc.id
}

output "id_public_subnets" {
  value = aws_subnet.public[*].id
}

output "id_private_subnets" {
  value = aws_subnet.private[*].id
}
