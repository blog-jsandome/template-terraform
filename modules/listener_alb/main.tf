

resource "aws_alb_listener" "listener_alb" {
  load_balancer_arn = var.alb_arn
  port              = var.port
  protocol          = var.protocol
  ssl_policy        = var.ssl_policy
  certificate_arn   = var.cert_arn
  default_action {
    type             = var.type_action
    target_group_arn = var.target_group_arn

  }
}
