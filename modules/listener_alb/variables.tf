

variable "alb_arn" {
  type        = string
  description = "The arn alb"
}

variable "port" {
  type        = string
  description = "The port listen"
}

variable "protocol" {
  type        = string
  description = "protocol for the port"
}

variable "cert_arn" {
  type        = string
  description = "Certificate when use HTTPS"
  default     = ""
}

variable "ssl_policy" {
  type        = string
  description = "Policy for security when use HTTPS -> ELBSecurityPolicy-2016-08"
  default     = ""
}

variable "type_action" {
  type        = string
  description = "forward default"
  default     = "forward"
}
variable "target_group_arn" {
  type        = string
  description = "Target group"
}
