output "alb_id" {
  value = aws_alb.application_load_balancer.id
}

output "alb_arn" {
  value = aws_alb.application_load_balancer.arn
}
