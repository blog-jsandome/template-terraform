resource "aws_alb" "application_load_balancer" {
  name               = "${var.app_name}-${var.resource}"
  internal           = false
  load_balancer_type = "application"
  subnets            = var.public_subnets  # need to create network before run this process
  security_groups    = var.security_groups # need to create SG before run this process

  tags = {
    Name        = "${var.app_name}-${var.resource}"
    Environment = var.app_environment
    Resource    = var.resource
    Project     = var.app_name
  }
}
