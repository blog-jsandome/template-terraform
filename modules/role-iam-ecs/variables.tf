# APP vars
variable "app_name" {
  type        = string
  description = "Application Name"
}

variable "app_environment" {
  type        = string
  description = "Application Environment"
}

variable "resource" {
  type        = string
  description = "resource for example ALB, ECS, EC2"
}

variable "project" {
  type        = string
  description = "Name project"
}

variable "secretsmanager_id" {
  type        = string
  description = "ID secret manager"
}

variable "name_bucket" {
  type        = string
  description = "Name bucket for vars"
}
