resource "aws_iam_role" "ecsTaskExecutionRole" {
  name               = "${var.app_name}-${var.app_environment}-execution-task-role"
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy.json

  inline_policy {
    name = "s3-envfile-ecs"

    policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Action   = ["s3:GetObject"]
          Effect   = "Allow"
          Resource = ["arn:aws:s3:::${var.name_bucket}/*"]
        },
        {
          Effect   = "Allow"
          Action   = ["s3:GetBucketLocation"]
          Resource = ["arn:aws:s3:::${var.name_bucket}"]
        },
      ]
    })
  }


  inline_policy {
    name = "private-registry-git"

    policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Action   = ["kms:Decrypt", "secretsmanager:GetSecretValue"]
          Effect   = "Allow"
          Resource = ["${var.secretsmanager_id}"]
        },
      ]
    })
  }

  inline_policy {
    name = "default-po"

    policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Action = ["ecr:GetAuthorizationToken",
            "ecr:BatchCheckLayerAvailability",
            "ecr:GetDownloadUrlForLayer",
            "ecr:BatchGetImage",
            "logs:CreateLogStream",
          "logs:PutLogEvents"]
          Effect   = "Allow"
          Resource = ["*"]
        },
      ]
    })
  }

  tags = {
    Name        = "${var.app_name}-iam-role"
    Environment = var.app_environment
    Project     = var.project
    Resource    = var.resource
  }
}

data "aws_iam_policy_document" "assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role_policy_attachment" "ecsTaskExecutionRole_policy" {
  role       = aws_iam_role.ecsTaskExecutionRole.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}
